# Check MR
Gitlab poor man's notification.
Get notification when gitlab MR is ready to merge.


## Install

You need `notify-senf`. To install in a debian base distro:

``` bash
sudo apt-get install libnotify-bin
```

To install the python dependencies you can:

``` bash
sudo apt-get install python3-requests python3-toml
```

Or with pip:

``` bash
pip install requests toml
```

## Config

The first time you need to configure TOKEN and BASE_URL. This can be done with:

``` bash
python check_mr.py --token PRIVATE-TOKEN --base-url BASE-URL
```
The config live in: "~/.check_mr.toml" and the logs live in: "~/check_mr.log".
See the log if you have problems.

## Usage

Just run:

``` bash
python check_mr.py
```

For more help:

``` bash
python check_mr.py --help
```
