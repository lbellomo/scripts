import os
import sys
import logging
import argparse
from time import sleep
from pathlib import Path
from urllib.parse import urljoin

import toml
import requests

parser = argparse.ArgumentParser(
    description="""Gitlab poor man's notification,
    get notification when gitlab MR is ready to merge.

    The config live in: "~/.check_mr.toml" and
    the logs live in: "~/check_mr.log". """
)
parser.add_argument("--token", type=str, help="Configure Gitlab private token.")
parser.add_argument(
    "--base-url",
    dest="base_url",
    type=str,
    help='Configure base url of the domain, with the [http|https]. Example: "https://gitlab.com"',
)
parser.add_argument(
    "--min-upvotes",
    dest="min_upvotes",
    type=int,
    help="Configure minumun amount of upvotes. Defaut: 2",
)
parser.add_argument(
    "--expire-time",
    dest="expire_time",
    type=int,
    help="Configure notification lifetime in milisecond. Defaut 5000 (5 seconds)",
)
parser.add_argument(
    "--summary",
    type=str,
    help="Configure the title of the notification. Defaut: MR Ready to merge!",
)

args = parser.parse_args()
path_log = Path("~/.check_mr.log")
path_config = Path("~/.check_mr.toml")
logging.basicConfig(filename=path_log.expanduser(), level=logging.INFO)


try:
    config = toml.load(path_config.expanduser())
except FileNotFoundError:
    config = {}

old_config = config.copy()

keys = ["base_url", "token", "min_upvotes", "expire_time", "summary"]
default_values = ["", "", 2, 5000, "MR Ready to merge!"]
args_values = [
    args.base_url,
    args.token,
    args.min_upvotes,
    args.expire_time,
    args.summary,
]

for k, d_v, v in zip(keys, default_values, args_values):
    if v:
        config[k] = v
        logging.info(f'Set key "{k}" to "{v}".')
    elif k not in config:
        config[k] = d_v
        logging.info(f'Set defaut key "{k}" to "{d_v}".')

if config != old_config:
    with open(path_config.expanduser(), "w") as f:
        toml.dump(config, f)
    logging.info(f"Write config to {path_config}.")

for k in ["base_url", "token"]:
    if not config[k]:
        logging.error(f'Mandatory key "{k}" not found in config.')
        sys.exit(1)

# config
min_upvotes = config["min_upvotes"]
expire_time = config["expire_time"]
summary = config["summary"]
token = config["token"]
base_url = config["base_url"]

headers = {"Private-Token": token}
url = urljoin(base_url, "/api/v4/merge_requests")
params = {"state": "opened"}


know_ids = set()

if __name__ == "__main__":
    while True:

        try:

            r = requests.get(url, headers=headers, params=params, allow_redirects=False)
            data = r.json()
        except requests.exceptions.ConnectionError:
            logging.debug("Connection error. sleep a minute...")
            sleep(60)
            continue

        for d in data:
            if d["upvotes"] >= min_upvotes and d["id"] not in know_ids:
                web_url = d["web_url"]
                title = d["title"]
                source_branch = d["source_branch"]
                repo = web_url.rsplit("/", maxsplit=3)[1]

                logging.info(
                    f"title: {title}. "
                    f"repo: {repo}. "
                    f"source branch: {source_branch}. "
                    f"url: {web_url}."
                )

                know_ids.add(d["id"])

                clean_title = title.replace('"', "").replace("'", "").replace("/", "")
                body = f"Title:\t{clean_title}\nRepo:\t{repo}\nSource branch:\t{source_branch}"
                cmd = f'notify-send "{summary}" "{body}" -t {expire_time}'
                os.system(cmd)

        sleep(60)
